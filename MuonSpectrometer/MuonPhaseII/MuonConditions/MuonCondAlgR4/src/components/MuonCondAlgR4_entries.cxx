/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../ActsMuonAlignCondAlg.h"
#include "../MdtCalibDbAlg.h"

DECLARE_COMPONENT(MuonCalibR4::MdtCalibDbAlg)
DECLARE_COMPONENT(ActsMuonAlignCondAlg)
