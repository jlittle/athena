/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "TRT_DetectorTool.h"
#include "TRTDetectorFactory_Full.h" 
#include "TRTDetectorFactory_Lite.h" 

#include "GeoModelUtilities/GeoModelExperiment.h"

#include "GeoModelUtilities/DecodeVersionKey.h"
#include "RDBAccessSvc/IRDBAccessSvc.h"
#include "RDBAccessSvc/IRDBRecord.h"
#include "RDBAccessSvc/IRDBRecordset.h"

#include "DetDescrConditions/AlignableTransformContainer.h"
#include "TRT_ConditionsData/StrawDxContainer.h" 

#include "AthenaKernel/ClassID_traits.h"
#include "SGTools/DataProxy.h"

/////////////////////////////////// Constructor //////////////////////////////////
//
TRT_DetectorTool::TRT_DetectorTool( const std::string& type, const std::string& name, const IInterface* parent )
  : GeoModelTool( type, name, parent )
{
}

//////////////  Create the Detector Node corresponding to this tool //////////////
//
StatusCode TRT_DetectorTool::create()
{ 
  // Get the detector configuration.
  ATH_CHECK( m_geoDbTagSvc.retrieve());

  // Get the straw status tool
  ATH_CHECK(m_sumTool.retrieve());
  
  ServiceHandle<IRDBAccessSvc> accessSvc(m_geoDbTagSvc->getParamSvcName(),name());
  ATH_CHECK( accessSvc.retrieve());

  // Locate the top level experiment node 
  GeoModelExperiment* theExpt{nullptr};
  ATH_CHECK(detStore()->retrieve(theExpt,"ATLAS"));
  GeoPhysVol *world = theExpt->getPhysVol();
  
  // Retrieve the Geometry DB Interface
  ATH_CHECK( m_geometryDBSvc.retrieve() );

  // Pass athena services to factory, etc
  m_athenaComps.setDetStore(detStore().operator->());
  m_athenaComps.setGeoDbTagSvc(m_geoDbTagSvc.get());
  m_athenaComps.setRDBAccessSvc(accessSvc.get());
  m_athenaComps.setGeometryDBSvc(m_geometryDBSvc.get());

  GeoModelIO::ReadGeoModel* sqliteReader  = m_geoDbTagSvc->getSqliteReader();
  //
  // If we are using the SQLite reader, then we are not building the raw geometry but
  // just locating it and attaching to readout geometry and various other actions 
  // taken in this factory.
  //
  if (sqliteReader) {
    ATH_MSG_INFO( " Building TRT geometry from GeoModel factory TRTDetectorFactory_Lite" );
    TRTDetectorFactory_Lite theTRTFactory(sqliteReader,
					  &m_athenaComps,
					  m_sumTool.get(),
					  m_useOldActiveGasMixture,
					  m_DC2CompatibleBarrelCoordinates,
					  m_overridedigversion,
					  m_alignable,
					  m_useDynamicAlignFolders
					  );

    theTRTFactory.create(world);
    m_manager=theTRTFactory.getDetectorManager();
  }
  else {
    DecodeVersionKey versionKey(m_geoDbTagSvc.get(), "TRT");

    ATH_MSG_INFO( "Building TRT with Version Tag: "<< versionKey.tag() << " at Node: " << versionKey.node() );

    // Print the TRT version tag:
    std::string trtVersionTag = accessSvc->getChildTag("TRT", versionKey.tag(), versionKey.node());
    ATH_MSG_INFO("TRT Version: " << trtVersionTag );
    
    // Check if version is empty. If so, then the TRT cannot be built. This may or may not be intentional. We
    // just issue an INFO message. 
    if (trtVersionTag.empty()) { 
      ATH_MSG_INFO("No TRT Version. TRT will not be built." );
      return StatusCode::SUCCESS;
    }
    
    ATH_MSG_DEBUG( "Keys for TRT Switches are "  << versionKey.tag()  << "  " << versionKey.node() );
    IRDBRecordset_ptr switchSet =  accessSvc->getRecordsetPtr("TRTSwitches", versionKey.tag(), versionKey.node());
    const IRDBRecord    *switches   = (*switchSet)[0];
      
    if (switches->getInt("DC1COMPATIBLE")) {
      ATH_MSG_ERROR( "DC1COMPATIBLE flag set in database, but DC1 is no longer supported in the code!!");
      return StatusCode::FAILURE;
    }

    m_DC2CompatibleBarrelCoordinates = switches->getInt("DC2COMPATIBLE");
    m_useOldActiveGasMixture         = ( switches->getInt("GASVERSION") == 0 );
    m_initialLayout                  = switches->getInt("INITIALLAYOUT");
      
    // Check if the new switches exists:
    if (m_doArgonMixture || m_doKryptonMixture ){
      if      ( switches->getInt("DOARGONMIXTURE") == 0) { m_doArgonMixture = false; }
      else if ( switches->getInt("DOARGONMIXTURE") == 1) { m_doArgonMixture = true; }

      if      ( switches->getInt("DOKRYPTONMIXTURE") == 0) { m_doKryptonMixture = false; }
      else if ( switches->getInt("DOKRYPTONMIXTURE") == 1) { m_doKryptonMixture = true; }
    }

    ATH_MSG_INFO( "Creating the TRT" );
    ATH_MSG_INFO( "TRT Geometry Options:" << std::boolalpha );
    ATH_MSG_INFO( "  UseOldActiveGasMixture         = " << m_useOldActiveGasMixture );
    ATH_MSG_INFO( "  Do Argon    = " << m_doArgonMixture );
    ATH_MSG_INFO( "  Do Krypton  = " << m_doKryptonMixture );
    ATH_MSG_INFO( "  DC2CompatibleBarrelCoordinates = " << m_DC2CompatibleBarrelCoordinates );
    ATH_MSG_INFO( "  InitialLayout                  = " << m_initialLayout );
    ATH_MSG_INFO( "  Alignable                      = " << m_alignable );
    ATH_MSG_INFO( "  VersioName                     = " << switches->getString("VERSIONNAME") );

    ATH_MSG_INFO( " Building TRT geometry from GeoModel factory TRTDetectorFactory_Full" );

    TRTDetectorFactory_Full theTRTFactory(&m_athenaComps,
					  m_sumTool.get(),
					  m_useOldActiveGasMixture,
					  m_DC2CompatibleBarrelCoordinates,
					  m_overridedigversion,
					  m_alignable,
					  m_doArgonMixture,
					  m_doKryptonMixture,
					  m_useDynamicAlignFolders
					  );
    theTRTFactory.create(world);
    m_manager=theTRTFactory.getDetectorManager();

  }

  // Register the TRTDetectorNode instance with the Transient Detector Store
  if (!m_manager) return StatusCode::FAILURE;

  theExpt->addManager(m_manager);
  ATH_CHECK(detStore()->record(m_manager,m_manager->getName()));
  return StatusCode::SUCCESS;
}



StatusCode 
TRT_DetectorTool::registerCallback ATLAS_NOT_THREAD_SAFE () // Thread unsafe StoreGateSvc::regFcn method and DataHandle template are used.
{
  // This callback is kept because the folder never changes.

  MsgStream log(msgSvc(), name());

  // If we fail to register any callbacks we return FAILURE. This just tells GeoModelSvc that 
  // no callbacks were registered. It will continue normally but without any alignments.
  StatusCode sc = StatusCode::FAILURE;

  if (m_alignable) {

    
    if (m_useDynamicAlignFolders){ // Regular alignment new schema   
      std::string folderName = "/TRT/AlignL1/TRT";
      if (detStore()->contains<CondAttrListCollection>(folderName)) {
        msg(MSG::DEBUG) << "Registering callback on global Container with folder " << folderName << endmsg;
        const DataHandle<CondAttrListCollection> calc;
        StatusCode trttmp = detStore()->regFcn(&IGeoModelTool::align, dynamic_cast<IGeoModelTool*>(this), calc, folderName);
        // We don't expect this to fail as we have already checked that the detstore contains the object. 
        if (trttmp.isFailure()) {
          msg(MSG::ERROR) << "Problem when register callback on global Container with folder " << folderName <<endmsg;
        } else {
          sc =  StatusCode::SUCCESS;
        }
      } else {
	msg(MSG::WARNING) << "Unable to register callback on global Container with folder " << folderName <<endmsg;
	return StatusCode::FAILURE;
      }
	
      folderName = "/TRT/AlignL2";
      if (detStore()->contains<AlignableTransformContainer>(folderName)) {
        if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Registering callback on AlignableTransformContainer with folder " << folderName << endmsg;
        const DataHandle<AlignableTransformContainer> atc;
        StatusCode sctmp = detStore()->regFcn(&IGeoModelTool::align, dynamic_cast<IGeoModelTool *>(this), atc, folderName);
        if(sctmp.isFailure()) {
          msg(MSG::ERROR) << "Problem when register callback on AlignableTransformContainer with folder " << folderName <<endmsg;
        } else {
          sc =  StatusCode::SUCCESS;
        }
      }
      else {
	msg(MSG::WARNING) << "Unable to register callback on AlignableTransformContainer with folder "
			  << folderName <<  endmsg;
	return StatusCode::FAILURE;
      }
    }
    else {  // Regular alignment old schema
      std::string folderName = "/TRT/Align";
      if (detStore()->contains<AlignableTransformContainer>(folderName)) {
	msg(MSG::DEBUG) << "Registering callback on AlignableTransformContainer with folder " << folderName << endmsg;
	const DataHandle<AlignableTransformContainer> atc;
	StatusCode sctmp = detStore()->regFcn(&IGeoModelTool::align, dynamic_cast<IGeoModelTool *>(this), atc, folderName);
	// We don't expect this to fail as we have already checked that the detstore contains the object.
	if (sctmp.isFailure()) {
	  msg(MSG::ERROR) << "Problem when register callback on AlignableTransformContainer with folder " << folderName <<endmsg;
	} else {
	  sc =  StatusCode::SUCCESS;
	}
      } else {
	msg(MSG::WARNING) << "Unable to register callback on AlignableTransformContainer with folder "
			  << folderName << ", Alignments disabled! (Only if no Run2 schema is loaded)" << endmsg;
      }
    }
  

    // Fine alignment
    {
      std::string folderName = "/TRT/Calib/DX";
      if (detStore()->contains<TRTCond::StrawDxContainer>(folderName)) {
        msg(MSG::DEBUG) << "Registering callback on StrawDxContainer with folder " << folderName << endmsg;
        const DataHandle<TRTCond::StrawDxContainer> sdc;
	StatusCode sctmp = detStore()->regFcn(&IGeoModelTool::align, dynamic_cast<IGeoModelTool*>(this), sdc, folderName);
	// We don't expect this to fail as we have already checked that the detstore contains the object.
	if (sctmp.isFailure()) {
	  msg(MSG::ERROR) << "Problem when register callback on StrawDxContainer with folder " << folderName <<endmsg;
	} else {
	  sc =  StatusCode::SUCCESS;
	}
      } else {
        msg(MSG::DEBUG) << "Unable to register callback on StrawDxContainer with folder " << folderName <<endmsg;
      }
    }    
   
  } else {
    msg(MSG::INFO) << "Alignment disabled. No callback registered" << endmsg;
    // We return failure otherwise it will try and register
    // a GeoModelSvc callback associated with this callback.
  }

  return sc;
}

StatusCode TRT_DetectorTool::clear()
{
  SG::DataProxy* proxy = detStore()->proxy(ClassID_traits<InDetDD::TRT_DetectorManager>::ID(),m_manager->getName());
  if(proxy) {
    proxy->reset();
    m_manager = nullptr;
  }
  return StatusCode::SUCCESS;
}

  
StatusCode 
TRT_DetectorTool::align(IOVSVC_CALLBACK_ARGS_P(I,keys))
{
  MsgStream log(msgSvc(), name()); 
  if (!m_manager) { 
    msg(MSG::WARNING) << "Manager does not exist" << endmsg;
    return StatusCode::FAILURE;
  }    
  if (m_alignable) {     
    return m_manager->align(I,keys);
  } else {
    msg(MSG::DEBUG) << "Alignment disabled. No alignments applied" << endmsg;
    return StatusCode::SUCCESS;
  }
}
